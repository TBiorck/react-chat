import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom'

import Chat from "./components/chat/Chat"
import Join from "./components/join/Join";
import Login from './components/login/Login';


function App() {
  return (
    <div className="App">
      <Router>
        <Route path="/" exact component={Join} />
        <Route path="/login" component={Login} />
        <Route path="/chat" component={Chat} />
      </Router>
    </div>
  );
}

export default App;
