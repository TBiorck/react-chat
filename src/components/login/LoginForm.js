import React, { useState } from 'react'

const LoginForm = (props) => {

  const [name, setName] = useState('')
  const [isLoading, setIsLoading] = useState(false)

  const onLoginClick = async () => {
    setIsLoading(true)

    let loginResult
    try {
      loginResult = await fetch('http://localhost:3001/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ name: name })
      }).then(r => r.json())
    } catch (e) {
      console.error(e);
    } finally {
      setIsLoading(false)
      props.complete(loginResult)
    }
  }

  const onNameChange = event => setName(event.target.value.trim()) 

  return (
    <form>
      <div className="form-group">
        <input className="form-control" type="text" placeholder="Enter a username..." onChange={ onNameChange } required></input>
      </div>
      <div>
        <button type="button" className="btn btn-success btn-lg" onClick={ onLoginClick }>Login</button>
      </div>

      { isLoading && <p>Logging in...</p>}
    </form>
  )
}

export default LoginForm
