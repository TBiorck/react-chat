import React from 'react'
import { useHistory, Redirect } from 'react-router-dom'
import LoginForm from "./LoginForm";
import { setStorage, getStorage } from '../../utils/localStorage'

const Login = () => {
  const token = getStorage('rc_jwt')
  const history = useHistory()

  const onLoginComplete = ({name, accessToken}) => {
    console.log(`TOKEN: ${accessToken}`);
    console.log(`NAME: ${name}`);
    setStorage('rc_jwt', accessToken)
    setStorage('name', name)

    console.log(getStorage('rc_jwt'))

    history.replace('/')
  }

  return (
    <div className="container-sm pt-3">
      { token && <Redirect to="/" /> }
      <h1>Login to React Chat</h1>

      <LoginForm complete={ onLoginComplete } />
    </div>
  )
}

export default Login