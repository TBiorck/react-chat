import React from 'react'

const Input = ({ message, setMessage, sendMessage }) => (
  <form>
    <div className="form-group">
      <input 
        type="text" 
        className="form-control" 
        placeholder="Enter a message..." 
        value={message} 
        onChange={ event => setMessage(event.target.value) } 
        onKeyPress={ event => event.key === 'Enter' ? sendMessage(event) : null }/>
    </div>
  </form>
)

export default Input