import React, { useState, useEffect } from 'react'
import queryString from 'query-string'
import io from 'socket.io-client'

import TopBar from './TopBar'
import Input from './Input'
import Messages from './Messages'
import UserList from '../lists/UserList'
import { getStorage } from '../../utils/localStorage'
import { Redirect } from 'react-router-dom'

let socket


const Chat = ({ location }) => {
  const token = getStorage('rc_jwt')

  const [name, setName] = useState('')
  const [room, setRoom] = useState('')
  const [message, setMessage] = useState('')
  const [messages, setMessages] = useState([])
  const [users, setUsers] = useState([])

  const API = 'http://localhost:3001'

  useEffect(() => {
    const { room } = queryString.parse(location.search)
    const name = getStorage('name')

    socket = io(API, {
      query: { token: getStorage('rc_jwt') }
    })

    setName(name)
    setRoom(room)

    socket.emit('joinRoom', { name, room }, (error) => {
      // TODO: implement callback if error occurs
      console.log(error);
    })

    return () => {
      socket.emit('disconnect', {room})

      socket.off()
    }
  }, [API, location.search])

  // On recieve data
  useEffect(() => {
    socket.on('message', (message) => {
      setMessages(msgs => [...msgs, message])
    })

    socket.on('roomData', ({ users }) => {
      setUsers(users)
    })

    socket.on('error', err => {
      console.log(err)
    })
  }, [])

  const sendMessage = (event) => {
    event.preventDefault()

    if (message) {
      socket.emit('sendMessage', { message, room }, () => setMessage(''))
    }
  }

  return (
    <div className="mt-2">
      { !token && <Redirect to="/login" /> }
      <TopBar room={room} />

      <div className="row">

        <div className="col">
          <div className="row">
            <div className="col pre-scrollable">
              <Messages messages={messages} name={name} />
            </div>
          </div>

          <div className="row">
            <div className="col">
              <Input message={message} setMessage={setMessage} sendMessage={sendMessage} />
            </div>
          </div>
        </div>

        {/* <div className="col-3 border">
          <UserList users={users} />
        </div> */}

      </div>
    </div>
  )
}

export default Chat