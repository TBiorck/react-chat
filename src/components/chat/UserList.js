import React from 'react'

const UserList = ( {users} ) => {

  const htmlUserList = []
  if (users) {
    for (let i = 0; i < users.length; i++) {
      const item = <li key={i} className="list-group-item">{users[i]}</li>
      htmlUserList.push(item)
    }
  }

  return (
    <div>
      <h5>Users in room</h5>
      <ul className="list-group">
        {htmlUserList}
      </ul>
    </div>
  )
}

export default UserList