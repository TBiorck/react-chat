import React from 'react'
import { Navbar, Nav, Button } from "react-bootstrap";

import onlineIcon from '../../assets/icons/onlineIcon.png'

const TopBar = ({ room }) => (
  <Navbar collapseOnSelect expand="lg" bg="primary" variant="dark">
    <Navbar.Brand>
      <img src={onlineIcon} className="mr-2" alt="online icon" />
      { room }
    </Navbar.Brand>

    <Nav className="mr-auto">
    </Nav>

    <Nav>
      <Button href="/" variant="danger">X</Button>
    </Nav>
  </Navbar>
)

export default TopBar