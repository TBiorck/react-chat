import React from 'react'

const Message = ({ message: { user, text }, name }) => {
  let sentByUser = false

  // The username gets trimmed and made to lowercase on server
  const trimmedName = name.trim().toLowerCase()

  if (user === trimmedName) {
    sentByUser = true
  }

  return (
    sentByUser ?
      (
        <div className="row mr-3 ml-3">
          <div className="col-8"></div>
          <div className="col">
            <p className="bg-primary text-white border rounded text-left p-1"><b>{trimmedName}</b>: {text}</p>
          </div>
        </div>
      ) :
      (
        <div className="row mr-3 ml-3">
          <div className="col">
            <p className="bg-light border rounded text-left p-1">{user}: {text}</p>
          </div>
          <div className="col-8"></div>
        </div>
      )
  )
}

export default Message