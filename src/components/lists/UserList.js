import React from 'react'

const UserList = ({users}) => {

  return (
    <div className="userList">
      <ul className="list-group">
        { users.map(user => (
          <li className="list-group-item text-left border border-success rounded mb-1" key={ user.id }>{user.name}</li>
        )) }
      </ul>
    </div>
  )
}

export default UserList