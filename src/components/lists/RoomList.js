import React from 'react'
import { Link } from 'react-router-dom'

const RoomList = ({ rooms }) => (
    <div className="roomsList">
        <ul className="list-group">
            {rooms.map(room => (
                <li className="list-group-item text-left border border-success rounded mb-1" key={room}>
                    <Link to={`/chat?room=${ room }`}>{ room }</Link>
                </li>
            ))}
        </ul>
    </div>
)

export default RoomList