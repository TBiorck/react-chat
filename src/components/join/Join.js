import React, { useState, useEffect } from 'react'
import { Link, Redirect } from "react-router-dom";
import io from 'socket.io-client'
import { Container } from "react-bootstrap";

import { getStorage } from '../../utils/localStorage';
import UserList from '../lists/UserList'
import RoomList from '../lists/RoomList'

let socket

const Join = () => {
  const token = getStorage('rc_jwt')

  const [users, setUsers] = useState([])
  const [rooms, setRooms] = useState([])

  const API = 'http://localhost:3001'


  useEffect(() => {
    const name = getStorage('name')

    socket = io(API, {
      query: { token: getStorage('rc_jwt') }
    })

    socket.emit('join', { name }, () => {
      // TODO: implement callback if error occurs
    })

    return () => {
      socket.emit('disconnect')

      socket.off()
    }

  }, [API])

  useEffect(() => {
    socket.on('usersData', ({ users }) => {
      console.log(users);
      setUsers(users)
    })

    socket.on('roomsData', ({ rooms }) => {
      console.log('here');
      setRooms(rooms)
    })
  }, [])

  return (
    <Container>
      {!token && <Redirect to="/login" />}
      <h1 className="mb-5">Join the Chat</h1>
      <div className="row d-flex justify-content-center">
        <div className="col-md-4 border border rounded bg-light p-2 mr-2">
          <h3>Online Users</h3>
          <UserList users={users} />
        </div>

        <div className="col-md-4 border border rounded bg-light p-2 ml-2">
          <h3>Rooms</h3>
          <RoomList rooms={rooms}/>
        </div>

      </div>

    </Container>
  )
}

export default Join